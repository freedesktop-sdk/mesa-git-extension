variables:
  IMAGE_ID: 'latest'
  # Docker Images
  DOCKER_REGISTRY: "registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images"

  XDG_CACHE_HOME: "${CI_PROJECT_DIR}/cache"

  FF_TIMESTAMPS: 1

  # mesa-git-extension is only available through Flathub beta
  RELEASE_CHANNEL: beta

  TRACK_ELEMENTS: track-elements.bst

default:
  image: "${DOCKER_REGISTRY}/bst2:${IMAGE_ID}"
  interruptible: true
  after_script:
  - rm -rf "${XDG_CACHE_HOME}/buildstream/artifacts"
  - rm -rf "${XDG_CACHE_HOME}/buildstream/build"
  - rm -rf "${XDG_CACHE_HOME}/buildstream/cas"
  - rm -rf "${XDG_CACHE_HOME}/buildstream/sources"
  - rm -rf "${CI_PROJECT_DIR}/.bst"
  retry:
    max: 2
    when: runner_system_failure

  id_tokens:
    CACHE_TOKEN:
      aud: cache.freedesktop-sdk.io

stages:
- build
- publish
- update

workflow:
  rules:
  - if: '$CI_MERGE_REQUEST_IID'
  - if: '$CI_COMMIT_BRANCH && $CI_COMMIT_REF_PROTECTED == "true"'
  - if: '$CI_COMMIT_TAG'
  - if: '$CI_PIPELINE_SOURCE == "schedule"'

before_script:
  - |
    export REPO_TOKEN="${FLATHUB_REPO_TOKEN_BETA}"

  - |
    if cp --reflink=always README.md .reflink-test; then
    echo Reflinks supported, disabling buildbox-fuse
    chmod -x /usr/bin/buildbox-fuse
    rm .reflink-test
    fi

  # Private SSL keys/certs for pushing to the CAS server
  - |
    [ -d ~/.config ] || mkdir -p ~/.config
    echo $CACHE_TOKEN > ~/.config/freedesktop-sdk.token
    [ -S /run/casd/casd.sock ] && export LOCAL_CAS=true
    python3 -mmako.cmd templates/buildstream.conf --output-file ~/.config/buildstream.conf
    cat ~/.config/buildstream.conf


.merge_request_build:
  stage: build
  rules:
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
  script:
  - |
    make build ARCH=${ARCH}
  - |
    if [[ "${ARCH}" = "x86_64" ]]; then
    make build ARCH=i686
    fi
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs

.builder-x86_64:
  tags:
  - x86_64
  variables:
    ARCH: x86_64

.builder-aarch64:
  tags:
  - aarch64
  variables:
    ARCH: aarch64

.builder-ppc64le:
  tags:
  - ppc64le
  variables:
    ARCH: ppc64le

lint:
  stage: build
  script:
    - ruff format --check utils/*.py
    - ruff check --output-format=gitlab utils/*.py
  rules:
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
  needs: []

build_x86_64:
  extends:
  - .merge_request_build
  - .builder-x86_64

build_aarch64:
  extends:
  - .merge_request_build
  - .builder-aarch64

build_ppc64le:
  extends:
  - .merge_request_build
  - .builder-ppc64le

publish_flatpak:
  stage: publish
  interruptible: false
  variables:
    DISABLE_CACHE_PUSH: 1
  script:
  - make export-repo ARCH=x86_64 BOOTSTRAP_ARCH=x86_64 REPO=repo
  - make export-repo ARCH=i686 BOOTSTRAP_ARCH=x86_64 REPO=repo
  - make export-repo ARCH=aarch64 BOOTSTRAP_ARCH=aarch64 REPO=repo
  - |
    for ref in $(ostree --repo=repo refs --list); do
      case "${ref}" in
        */*/i386/*)
          echo "Deleting ${ref}"
          ostree --repo=repo refs --delete "${ref}"
          ;;
        */*/*/*)
          echo "Keeping ${ref}"
          ;;
      esac
    done
  - python utils/validate_repo.py --path repo
  - flatpak build-update-repo --generate-static-deltas repo
  - flat-manager-client create https://hub.flathub.org/ "${RELEASE_CHANNEL}" --build-log-url ${CI_PIPELINE_URL} > publish_build.txt
  - flat-manager-client push $(cat publish_build.txt) repo --build-log-url ${CI_PIPELINE_URL}
  - flat-manager-client commit --wait $(cat publish_build.txt)
  - flat-manager-client publish --wait $(cat publish_build.txt)
  artifacts:
    when: always
    paths:
    - ${CI_PROJECT_DIR}/cache/buildstream/logs
  after_script:
  - |
    export REPO_TOKEN="${FLATHUB_REPO_TOKEN_BETA}"
    test -s publish_build.txt
    flat-manager-client purge "$(cat publish_build.txt)"
  rules:
  - if: '$CI_PIPELINE_SOURCE == "schedule"'
    when: never
  - if: '$CI_COMMIT_BRANCH == "master"'
  - if: '$CI_COMMIT_BRANCH =~ /^release\/.*/'

track_updates:
  stage: update
  rules:
  - if: '$CI_PIPELINE_SOURCE == "schedule"'
  script:
  - git remote set-url origin "https://gitlab-ci-token:${FREEDESKTOP_API_KEY}@gitlab.com/freedesktop-sdk/mesa-git-extension.git"
  - git config user.name "freedesktop_sdk_updater"
  - git config user.email "freedesktop_sdk_updater@libreml.com"
  - git branch -f "${CI_COMMIT_REF_NAME}" "origin/${CI_COMMIT_REF_NAME}"
  - git fetch --prune
  - if [[ "$CI_COMMIT_BRANCH" == "master" ]]; then python utils/cleanup_leftover_branches.py; fi
  - |
    case "${CI_COMMIT_REF_NAME}" in
      master)
      ;;
      release/*)
      ;;
      *)
        false
      ;;
    esac
  - GITLAB_TOKEN=$FREEDESKTOP_API_KEY auto_updater --verbose
    --base_branch "${CI_COMMIT_REF_NAME}"
    --nobuild
    --overwrite
    --push
    --create_mr
    --gitlab_project="freedesktop-sdk/mesa-git-extension"
    --max_merge_requests=4
    $TRACK_ELEMENTS
